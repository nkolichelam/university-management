import React, { Component } from "react";
import { Link } from "react-router-dom";

// Redux
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// Custom Actions
import UserActions from '../redux/actions/UserActions';

// START IMPORT ACTIONS

// END IMPORT ACTIONS

/** APIs

**/

class Home extends Component {
  render() {
    return (
      <div>
        <h2>Dashboard</h2>            
        <div>
          <Link to="#">Link to University List</Link>
        </div>
        <div>
          <Link to="#">Link to Department List</Link>
        </div>
        <div>
          <Link to="#">Link to Teacher List</Link>
        </div>
        <div>
          <Link to="#">Link to Student List</Link>
        </div>
        <div>
          <Link to="#">Link to Course List</Link>
        </div>
        {/* <div>
          <Link to="#">Link to examList</Link>
        </div> */}
            
      </div>
    );
  }
}

// Store actions
const mapDispatchToProps = function(dispatch) {
  return { 
    actionsUser: bindActionCreators(UserActions, dispatch)
  };
};

// Validate types
Home.propTypes = {
  actionsUser: PropTypes.object.isRequired
};

// Get props from state
function mapStateToProps(state, ownProps) {
  return {
    user: state.LoginReducer.user
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
